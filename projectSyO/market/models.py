from django.db import models

# Create your models here.
from django.db import models

# Create your models here.
from companys.models import Company


class Offer(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    title = models.CharField(max_length=45)
    description = models.CharField(max_length=250)
    technology = models.CharField(max_length=250)
    publication_date = models.DateField(auto_created=True, auto_now_add=True)

    class Meta:
        verbose_name = 'Offer'
        verbose_name_plural = 'Offers'

    def __str__(self):
        return self.title


class Demand(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    publication_date = models.DateField(auto_created=True, auto_now_add=True)
    title = models.CharField(max_length=45)
    description = models.CharField(max_length=250)
    requirements = models.CharField(max_length=250)
    taken = models.BooleanField

    class Meta:
        verbose_name = 'Demand'
        verbose_name_plural = 'Demands'

    def __str__(self):
        return self.title
