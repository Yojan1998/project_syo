
# Register your models here.
from market.models import Demand, Offer
from django.contrib import admin


#
# # Register your models here.

#
@admin.register(Offer)
class OfferAdmin(admin.ModelAdmin):
    list_display = ['company', 'title', 'description', 'technology','publication_date', ]
    list_display_links = ['company', 'title', 'description', 'technology','publication_date', ]
    raw_id_fields = ('company',)
    search_fields = ('title', 'company__company_name',)


@admin.register(Demand)
class DemandAdmin(admin.ModelAdmin):
    list_display = ['company', 'publication_date', 'title', 'description', 'requirements', ]
    list_display_links = ['company', 'publication_date', 'title', 'description', 'requirements', ]
    raw_id_fields = ('company',)
    search_fields = ('title', 'company__company_name',)
