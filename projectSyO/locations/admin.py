from django.contrib import admin

# Register your models here.
from django.contrib import admin

# Register your models here.
from locations.models import Country


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ['name', 'city', ]
    list_display_links = ['name', 'city', ]

    search_fields = ('name', 'city',)
