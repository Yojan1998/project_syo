from django.db import models

# Create your models here.
from django.db import models


# Create your models here.
class Country(models.Model):
    name = models.CharField(max_length=30)
    city = models.CharField(max_length=30)

    class Meta:
        verbose_name = 'Country'
        verbose_name_plural = 'Countrys'

    def __str__(self):
        return self.name
