from django.contrib import admin

# Register your models here.
from django.contrib import admin

# Register your models here.

from companys.models import Company


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ['company_name', 'location', 'address', 'phone', 'email', 'work_experience', 'logo', 'type',
                    'question1', 'answer1', 'question1', 'answer2', 'question2', 'answer3', 'question3', 'answer4',
                    'question4', 'answer5', 'question5', 'answer6', 'question6', 'answer7', 'question7', 'answer8',
                    'question8', 'answer9', 'question9', 'answer10', 'question10', 'answer11', 'question11', 'grafica1',
                    'grafica2', ]
    list_display_links = ['company_name', 'location', 'address', 'phone', 'email', 'work_experience', 'logo', 'type',
                          'question1', 'answer1', 'question1', 'answer2', 'question2', 'answer3', 'question3',
                          'answer4',
                          'question4', 'answer5', 'question5', 'answer6', 'question6', 'answer7', 'question7',
                          'answer8',
                          'question8', 'answer9', 'question9', 'answer10', 'question10', 'answer11', 'question11',
                          'grafica1', 'grafica2', ]
    search_fields = ('company_name',)


# @admin.register(Certification)
# class CertificationAdmin(admin.ModelAdmin):
#     list_display = ['company', 'certification', ]
#     list_display_links = ['company', 'certification', ]
#     raw_id_fields = ('company',)
#     search_fields = ('company', 'certification',)
#

# @admin.register(Question)
# class CertificationAdmin(admin.ModelAdmin):
#     list_display = ['company', 'question', ]
#     list_display_links = ['company', 'question', ]
#     raw_id_fields = ('company',)
#     search_fields = ('company', 'question',)


# @admin.register(Answer)
# class CertificationAdmin(admin.ModelAdmin):
#     list_display = ['company', 'question', 'answer', ]
#     list_display_links = ['company', 'question', 'answer', ]
#     raw_id_fields = ('company', 'question',)
#     search_fields = ('company', 'name',)
#
#
# @admin.register(Client)
# class CertificationAdmin(admin.ModelAdmin):
#     list_display = ['company', 'name_client', ]
#     list_display_links = ['company', 'name_client', ]
#     raw_id_fields = ('company',)
#     search_fields = ('company', 'name_client',)
#
#
# @admin.register(Service)
# class CertificationAdmin(admin.ModelAdmin):
#     list_display = ['company', 'service_name', ]
#     list_display_links = ['company', 'service_name', ]
#     raw_id_fields = ('company',)
#     search_fields = ('company', 'service_name',)
#
#
# @admin.register(Language)
# class CertificationAdmin(admin.ModelAdmin):
#     list_display = ['company', 'name_language', ]
#     list_display_links = ['company', 'name_language', ]
#     raw_id_fields = ('company',)
#     search_fields = ('company', 'name_language',)
