from django.urls import path

from companys.views import CompanyView, NationalCompanyListView, ForeignCompanyListView, \
    CompanyDetailView

urlpatterns = [
    path('empresa/<int:pk>', CompanyDetailView.as_view(), name='perfil-empresa'),
    path('nacionales', NationalCompanyListView.as_view(), name="empresas-nacionales"),
    path('extranjeras', ForeignCompanyListView.as_view(), name='empresas-extranjeras'),
    path('', CompanyView.as_view(), name='index'),

]
