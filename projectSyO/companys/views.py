from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView, ListView, DetailView

from companys.models import Company
from market.models import Offer, Demand


class CompanyView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['offers'] = Offer.objects.all()
        data['demands'] = Demand.objects.all()
        data['companys'] = Company.objects.all()
        return data


class NationalCompanyListView(ListView):
    template_name = 'empresas-nacionales.html'
    model = Company
    queryset = Company.objects.filter(type='Nacional')


class ForeignCompanyListView(ListView):
    template_name = 'empresas-extranjeras.html'
    model = Company
    queryset = Company.objects.filter(type='Extranjeras')


class CompanyDetailView(DetailView):
    template_name = 'perfiles-empresas.html'
    model = Company

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        pk_company = self.object
        data['offers'] = Offer.objects.filter(company__pk=pk_company.pk)
        data['demands'] = Demand.objects.filter(company__pk=pk_company.pk)
        return data

        # pk_company = self.object
        # data['offers'] = Offer.objects.all()
        # data['demandas'] = Demand.objects.all()
        # data['questions'] = Question.objects.filter(company__pk=pk_company.pk)
        # data['servicios'] = Service.objects.filter(company__pk=pk_company.pk)
        # data['answers'] = Answer.objects.filter(company__pk=pk_company.pk)
        # data['languajes'] = Language.objects.filter(company__pk=pk_company.pk)
        # data['certificacions'] = Certification.objects.filter(company__pk=pk_company.pk)
        # return data
