from django.db import models

# Create your models here.

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from locations.models import Country


class Company(models.Model):
    company_name = models.CharField(max_length=25)
    location = models.ForeignKey(Country, on_delete=models.CASCADE)
    address = models.CharField(max_length=45)
    phone = models.PositiveIntegerField()
    email = models.EmailField()
    work_experience = models.CharField(max_length=25,
                                       choices=(("1 a 3 años", "1 a 3 años"), ("3 a 6 años", "3 a 6 años"),
                                                ("6 a 15 años", "6 a 15 años"),
                                                ("15 en adelante", "15 en adelante")))

    question1 = models.CharField(max_length=250)
    answer1 = models.CharField(max_length=250)
    question2 = models.CharField(max_length=250)
    answer2 = models.CharField(max_length=250)
    question3 = models.CharField(max_length=250)
    answer3 = models.CharField(max_length=250)
    question4 = models.CharField(max_length=250)
    answer4 = models.CharField(max_length=250)
    question5 = models.CharField(max_length=250)
    answer5 = models.CharField(max_length=250)
    question6 = models.CharField(max_length=250)
    answer6 = models.CharField(max_length=250)
    question7 = models.CharField(max_length=250)
    answer7 = models.CharField(max_length=250)
    question8 = models.CharField(max_length=250)
    answer8 = models.CharField(max_length=250)
    question9 = models.CharField(max_length=250)
    answer9 = models.CharField(max_length=250)
    question10 = models.CharField(max_length=250)
    answer10 = models.CharField(max_length=250)
    question11 = models.CharField(max_length=250)
    answer11 = models.CharField(max_length=250)
    logo = models.ImageField()
    type = models.CharField(max_length=25, choices=(("Nacional", "Nacional"), ("Extranjeras", "Extranjeras")))
    grafica1 = models.ImageField()
    grafica2 = models.ImageField()

    class Meta:
        verbose_name = 'company'
        verbose_name_plural = 'companys'

    def __str__(self):
        return self.company_name

#
# class Certification(models.Model):
#     company = models.ForeignKey(Company, on_delete=models.CASCADE)
#     certification = models.CharField(max_length=45)
#
#     class Meta:
#         verbose_name = 'certification'
#         verbose_name_plural = 'certifications'
#
#     def __str__(self):
#         return self.certification
#
#
# class Question(models.Model):
#     company = models.ForeignKey(Company, on_delete=models.CASCADE)
#     question = models.CharField(max_length=250)
#
#     verbose_name = 'question'
#     verbose_name_plural = 'questions'
#
#     def __str__(self):
#         return self.question
#
#
# class Service(models.Model):
#     company = models.ForeignKey(Company, on_delete=models.CASCADE)
#     service_name = models.CharField(max_length=250)
#
#     class Meta:
#         verbose_name = 'service'
#         verbose_name_plural = 'services'
#
#     def __str__(self):
#         return self.service_name
#
#
# class Answer(models.Model):
#     company = models.ForeignKey(Company, on_delete=models.CASCADE)
#     question = models.ForeignKey(Question, on_delete=models.CASCADE)
#     answer = models.CharField(max_length=45)
#
#     class Meta:
#         verbose_name = 'answer'
#         verbose_name_plural = 'answers'
#
#     def __str__(self):
#         return self.answer
#
#
# class Client(models.Model):
#     company = models.ForeignKey(Company, on_delete=models.CASCADE)
#     name_client = models.CharField(max_length=100)
#
#     class Meta:
#         verbose_name = 'client'
#         verbose_name_plural = 'clients'
#
#     def __str__(self):
#         return self.name_client
#
#
# class Language(models.Model):
#     company = models.ForeignKey(Company, on_delete=models.CASCADE)
#     name_language = models.CharField(max_length=45)
#
#     class Meta:
#         verbose_name = 'language'
#         verbose_name_plural = 'languages'
#
#     def __str__(self):
#         return self.name_language
